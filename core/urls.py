from core import views
from django.urls import path

from .models import *

app_name = "core"
urlpatterns = [
    path('list/stadiums/',views.ListStadium.as_view(), name="listS"),
    path('create/Stadiums/',views.CreateStadium.as_view(), name="createS"),
    path('detail/Stadiums/<int:pk>/', views.DetailStadium.as_view(), name= "detailS"),
    path('update/Stadiums/<int:pk>/', views.UpdateStadium.as_view(), name= "UpdateS"),
    path('delete/Stadiums/<int:pk>/', views.DeleteStadium.as_view(), name= "deleteS"),
    
    path('list/Teams/',views.ListTeam.as_view(), name="listT"),
    path('create/Teams/',views.CreateTeam.as_view(), name="createT"),
    path('detail/Teams/<int:pk>/', views.DetailTeam.as_view(), name= "detailT"),
    path('update/Teams/<int:pk>/', views.UpdateTeam.as_view(), name= "UpdateT"),
    path('delete/Teams/<int:pk>/', views.DeleteTeam.as_view(), name= "deleteT"),
    
    path('list/Owners/',views.ListOwners.as_view(), name="listO"),
    path('create/Owners/',views.CreateOwners.as_view(), name="createO"),
    path('detail/Owners/<int:pk>/', views.DetailOwners.as_view(), name= "detailO"),
    path('update/Owners/<int:pk>/', views.UpdateOwners.as_view(), name= "UpdateO"),
    path('delete/Owners/<int:pk>/', views.DeleteOwners.as_view(), name= "deleteO"),
    
    path('list/Players/',views.ListPlayers.as_view(), name="listP"),
    path('list/TeamPlayers/',views.ListTeamsPlayers.as_view(), name="listTP"),
    path('create/Players/',views.CreatePlayers.as_view(), name="createP"),
    path('detail/Players/<int:pk>/', views.DetailPlayers.as_view(), name= "detailP"),
    path('update/Players/<int:pk>/', views.UpdatePlayers.as_view(), name= "UpdateP"),
    path('delete/Players/<int:pk>/', views.DeletePlayers.as_view(), name= "deleteP"),
]