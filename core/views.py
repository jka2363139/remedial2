from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views import generic

from .forms import *
from .models import Stadiums, Owner, Teams, Players

# Create your views here.

####Estadios

class ListStadium(generic.View):
    template_name = "core/listS.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Stadiums.objects.all()
        self.context = {
            "Stadiums": queryset  
        }
        return render(request, self.template_name, self.context)



class DetailStadium(generic.DetailView):
    template_name = "core/detailS.html"
    model = Stadiums

    def stadium_detail(request, codigo):
        stadium = get_object_or_404(Stadiums, codigo=codigo)
        return render(request, 'detailS.html', {'stadium': stadium})

class CreateStadium(generic.CreateView):
    template_name = "core/createS.html"
    model = Stadiums
    form_class = StadiumForm
    success_url = reverse_lazy("core:listS")


class UpdateStadium(generic.UpdateView):
    template_name = "core/UpdateS.html"
    model = Stadiums
    form_class = UpdateStadiumForm
    success_url = reverse_lazy("core:listS")

class DeleteStadium(generic.DeleteView):
    template_name = "core/deleteS.html"
    model = Stadiums
    success_url = reverse_lazy("core:listS")


####Equipos

class ListTeam(generic.View):
    template_name = "core/listT.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Teams.objects.all()
        self.context = {
            "Teams": queryset
        }
        return render(request, self.template_name,self.context)
    
class CreateTeam(generic.CreateView):
    template_name = "core/createT.html"
    model = Teams
    form_class = TeamForm
    success_url = reverse_lazy("core:listT")

    def form_valid(self, form):
        
        return super().form_valid(form)


class DetailTeam(generic.DetailView):
    template_name = "core/detailT.html"
    model = Teams
    
    def detail_team(request, pk):
        teams = get_object_or_404(teams, pk=pk)
        return render(request, 'detailT.html', {'Teams': teams})

class UpdateTeam(generic.UpdateView):
    template_name = "core/UpdateT.html"
    model = Teams
    form_class = UpdateTeamForm
    success_url = reverse_lazy("core:listT")

class DeleteTeam(generic.DeleteView):
    template_name = "core/deleteT.html"
    model = Teams
    success_url = reverse_lazy("core:listT")

####Dueños

class ListOwners(generic.View):
    template_name = "core/listO.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Owner.objects.all()
        self.context = {
            "Owners": queryset
        }
        return render(request, self.template_name,self.context)
    
class CreateOwners(generic.CreateView):
    template_name = "core/createO.html"
    model = Owner
    form_class = OwnersForm
    success_url = reverse_lazy("core:listO")

    def form_valid(self, form):
        return super().form_valid(form)


class DetailOwners(generic.DetailView):
    template_name = "core/detailO.html"
    model = Owner

    def detail_owner(request, pk):
        owner = get_object_or_404(owner, pk=pk)
        return render(request, 'detailO.html', {'Owner': owner})


class UpdateOwners(generic.UpdateView):
    template_name = "core/UpdateO.html"
    model = Owner
    form_class = UpdateOwnersForm
    success_url = reverse_lazy("core:listO")

class DeleteOwners(generic.DeleteView):
    template_name = "core/deleteO.html"
    model = Owner
    success_url = reverse_lazy("core:listO")

####Jugadores

class ListPlayers(generic.View):
    template_name = "core/listP.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Players.objects.all()
        self.context = {
            "Players": queryset
        }
        return render(request, self.template_name,self.context)
    
class CreatePlayers(generic.CreateView):
    template_name = "core/createP.html"
    model = Players
    form_class = PlayersForm
    success_url = reverse_lazy("core:listP")

    def form_valid(self, form):
        
        return super().form_valid(form)


class DetailPlayers(generic.DetailView):
    template_name = "core/detailP.html"
    model = Players

class UpdatePlayers(generic.UpdateView):
    template_name = "core/UpdateP.html"
    model = Players
    form_class = UpdatePlayersForm
    success_url = reverse_lazy("core:listP")

class DeletePlayers(generic.DeleteView):
    template_name = "core/deleteP.html"
    model = Players
    success_url = reverse_lazy("core:listP")
    
####Jugadores equipos

class ListTeamsPlayers(generic.View):
    template_name = "core/team_players.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Players.objects.all()
        self.context = {
            "Players": queryset
        }
        return render(request, self.template_name,self.context)