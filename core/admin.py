from django.contrib import admin

from .models import Stadiums, Teams, Owner, Players


@admin.register(Stadiums)
class StadiumAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
        "capacidad",
        "largo",
        "ancho",
        "propietario",
    ]

@admin.register(Teams)
class TeamAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
        "sede",
        "propietario",
    ]

@admin.register(Owner)
class OwnersAdmin(admin.ModelAdmin):
    list_display = [
        "numero",
        "nombre",
    ]

@admin.register(Players)
class PlayersAdmin(admin.ModelAdmin):
    list_display = [
        "numero",
        "nombreC",
        "posicion",
        "equipo",
    ]