from django import forms

from .models import Owner, Players, Stadiums, Teams

class OwnersForm(forms.ModelForm):
    class Meta:
        model = Owner
        fields = "__all__"
        Exclude=[]
        widgets = {
            "numero": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Ingrese el número del propietario"}),
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del propietario"}),
        }

class UpdateOwnersForm(forms.ModelForm):
    class Meta:
        model = Owner
        fields = "__all__"
        exclude=["numero"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nuevo nombre del propietario"}),
        }

class PlayersForm(forms.ModelForm):
    class Meta:
        model = Players
        fields = "__all__"
        exclude = []
        widgets = {
            "numero": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Ingrese el número del jugador"}),
            "nombreC": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre completo del jugador"}),
            "posicion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la posicion del jugador"}),
            "equipo": forms.Select(attrs={"class": "form-control"}),
        }

class UpdatePlayersForm(forms.ModelForm):
    class Meta:
        model = Players
        fields = "__all__"
        exclude = []
        widgets = {
            "numero": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Ingrese el nuevo número del jugador"}),
            "nombreC": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nuevo nombre del jugador"}),
            "posicion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la nueva posicion del jugador"}),
            "equipo": forms.Select(attrs={"class": "form-control"}),
        }


class StadiumForm(forms.ModelForm):
    class Meta:
        model = Stadiums
        fields = "__all__"
        exclude = []
        widgets = {
            "codigo": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el número del estadio"}),
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del estadio"}),
            "capacidad": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Ingrese la capacidad de personas del estadio"}),
            "largo": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Ingrese el Largo del estadio (yd)"}),
            "ancho": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Ingrese el Ancho del estadio (yd)"}),
            "propietario": forms.Select(attrs={"class": "form-control"}),
        }

class UpdateStadiumForm(forms.ModelForm):
    class Meta:
        model = Stadiums
        fields = "__all__"
        exclude = []
        widgets = {
            "codigo": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nuevo número del estadio"}),
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nuevo nombre del estadio"}),
            "capacidad": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Ingrese la nueva capacidad de personas del estadio"}),
            "largo": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Ingrese el nuevo Largo del estadio (yd)"}),
            "ancho": forms.NumberInput(attrs={"type": "number", "class": "form-control", "placeholder": "Ingrese el nuevo Ancho del estadio (yd)"}),
            "propietario": forms.Select(attrs={"class": "form-control"}),
        }

class TeamForm(forms.ModelForm):
    class Meta:
        model = Teams
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del equipo"}),
            "sede": forms.Textarea(attrs={"class": "form-control", "rows": 3, "placeholder": "Ingrese la descripción de la liga"}),
            "propietario": forms.Select(attrs={"class": "form-control"}),
        }

class UpdateTeamForm(forms.ModelForm):
    class Meta:
        model = Teams
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nuevo nombre del equipo"}),
            "sede": forms.Textarea(attrs={"class": "form-control", "rows": 3, "placeholder": "Ingrese la nueva descripción de la liga"}),
            "propietario": forms.Select(attrs={"class": "form-control"}),
        }

####class RelationshipForm(models.Model):
    #Stadium = models.ForeignKey(Stadium, on_delete=models.CASCADE)
    #Team = models.ForeignKey(Team, on_delete=models.CASCADE)
    #Players=models.ForeignKey(Players, on_delete=models.CASCADE)
    #Owners=models.ForeignKey(Owners, on_delete=models.CASCADE)
