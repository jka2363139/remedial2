from django.db import models

# Create your models here.

##Se crean modelos para poder trabajar con los requerimientos pedidos en la descripcion del programa
class Owner(models.Model):
    numero=models.IntegerField(primary_key=True)
    nombre=models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'Owners'
        
class Teams(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=128, blank=True, null=True)
    sede = models.CharField(max_length=256, blank=True, null=True)
    propietario=models.ForeignKey(Owner, on_delete=models.CASCADE, db_column='numero')

    class Meta:
        managed = True
        db_table = 'Team'    
    def __str__(self):
        return self.nombre

class Players(models.Model):
    numero=models.IntegerField(primary_key=True)
    nombreC=models.CharField(max_length=128, blank=True, null=True)
    posicion=models.CharField(max_length=64, blank=True, null=True)
    equipo=models.ForeignKey(Teams, on_delete=models.CASCADE, db_column='codigo')

    class Meta:
        managed = True
        db_table = 'Player'
    def __str__(self):
        return self.nombreC

class Stadiums(models.Model):
    codigo = models.IntegerField(primary_key=True)  
    nombre = models.CharField(max_length=128, blank=True, null=True)
    capacidad= models.IntegerField(blank=True, null=True)
    largo= models.IntegerField(blank=True, null=True)
    ancho=models.IntegerField(blank=True, null=True)
    propietario=models.ForeignKey(Owner, on_delete=models.CASCADE, db_column='numero')
    
    class Meta:
        managed = True
        db_table = 'Stadium'
    def __str__(self):
        return self.nombre